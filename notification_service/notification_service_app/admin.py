from django.contrib import admin
from notification_service_app.models import Client, Message, Notification, SendStatistics
from django.db.models import Count


class ClientAdmin(admin.ModelAdmin):
    list_display = ['phone_number', 'mobile_operator_code', 'tag', 'timezone']


admin.site.register(Client, ClientAdmin)


class NotificationAdmin(admin.ModelAdmin):
    list_display = ['start_time', 'end_time', 'message_text',
                    'client_filter_operator_code', 'client_filter_tag', 'status']


admin.site.register(Notification, NotificationAdmin)


@admin.register(SendStatistics)
class OrderSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/order_summary_change_list.html'

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response
        metrics = {
            'delivered': Count('delivered_at'),
            'created': Count('created_at'),
        }
        response.context_data['summary'] = list(
            qs
            .values('notification__start_time', 'notification__end_time')
            .annotate(**metrics)
            .order_by('-delivered')
        )
        return response
