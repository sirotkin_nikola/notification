from decouple import config
import requests
import datetime
from datetime import timezone
from .models import *
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


def send_message(data):
    message = Message.objects.create(notification=Notification.objects.get(id=data[3]),
                                     client=Client.objects.get(id=data[0]))

    url = config("URL")

    headers = {
        'Content-Type': 'application/json',
        'Authorization': f"Bearer {config('JWT_TOKEN')}",
    }
    json_data = {
        "id": data[0],
        "phone": data[1],
        "text": data[2]
    }
    response = requests.post(url, json=json_data, headers=headers, verify=False)
    if response.status_code == 200:
        message.status = 'Dv'
        message.delivered_at = datetime.datetime.now(timezone.utc)
        message.save()

        return response.status_code
    else:
        message.status = 'Er'
        message.error_message = response.status_code
        message.save()

        return response.status_code
