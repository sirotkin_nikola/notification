from .models import *
from .serializers import *
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from django.db.models import Count, Q
from drf_yasg import openapi
from rest_framework import status
from notification_service_app.utils import Statistics


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    @swagger_auto_schema(
        request_body=ClientSerializer,
        responses={200: ClientSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Create new client",
        operation_description="Create a new client an existing one.",
        tags=["client"],
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: ClientSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Get client list",
        operation_description="Get all client from database.",
        tags=["client"],
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: ClientSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Get one client",
        operation_description="Get one client by id .",
        tags=["client"],
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: ClientSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Update client",
        operation_description="Update one client by id .",
        tags=["client"],
    )
    def update(self, request, *args, **kwargs):
        return update().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: ClientSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Delete client",
        operation_description="Delete one client by id .",
        tags=["client"],
    )
    def destroy(self, request, *args, **kwargs):
        return update().destroy(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: ClientSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Update client field",
        operation_description="Updating an object client field by id .",
        tags=["client"],
    )
    def partial_update(self, request, *args, **kwargs):
        return update().partial_update(request, *args, **kwargs)


class MessageViewSet(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    @swagger_auto_schema(
        request_body=MessageSerializer,
        responses={200: MessageSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Message new message",
        operation_description="Create a new message an existing one.",
        tags=["message"],
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: MessageSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Get message list",
        operation_description="Get all message from database.",
        tags=["message"],
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: MessageSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Get one message",
        operation_description="Get one message by id .",
        tags=["message"],
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: MessageSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Update message",
        operation_description="Update one message by id .",
        tags=["message"],
    )
    def update(self, request, *args, **kwargs):
        return update().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: MessageSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Delete message",
        operation_description="Delete one message by id .",
        tags=["message"],
    )
    def destroy(self, request, *args, **kwargs):
        return update().destroy(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: MessageSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Update message field",
        operation_description="Updating an object message field by id .",
        tags=["message"],
    )
    def partial_update(self, request, *args, **kwargs):
        return update().partial_update(request, *args, **kwargs)


class NotificationViewSet(ModelViewSet):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer

    @swagger_auto_schema(
        request_body=NotificationSerializer,
        responses={200: NotificationSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Message new notification",
        operation_description="Create a new notification an existing one.",
        tags=["notification"],
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: NotificationSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Get notification list",
        operation_description="Get all notification from database.",
        tags=["notification"],
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: NotificationSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Get one notification",
        operation_description="Get one notification by id .",
        tags=["notification"],
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: NotificationSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Update notification",
        operation_description="Update one notification by id .",
        tags=["notification"],
    )
    def update(self, request, *args, **kwargs):
        return update().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: NotificationSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Delete notification",
        operation_description="Delete one notification by id .",
        tags=["notification"],
    )
    def destroy(self, request, *args, **kwargs):
        return update().destroy(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: NotificationSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Update notification field",
        operation_description="Updating an object notification field by id .",
        tags=["notification"],
    )
    def partial_update(self, request, *args, **kwargs):
        return update().partial_update(request, *args, **kwargs)


class StatisticsViewSet(APIView):
    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            required=['start_date_statistics', 'stop_date_statistics'],
            properties={
                'start_date_statistics': openapi.Schema(type=openapi.TYPE_STRING, format='date'),
                'stop_date_statistics': openapi.Schema(type=openapi.TYPE_STRING, format='date'),
            }
        ),
        responses={
            200: openapi.Response(
                description="Successful operation",
                content={'application/json': {'example': {'code': 200, 'status': 'Success'}}}
            ),
            400: openapi.Response(description="Bad Request"),
            404: openapi.Response(description="Not Found"),
        },
        operation_summary="Getting statistics",
        operation_description="Getting statistics on a date range",
        tags=["statistics"],
    )
    def post(self, request, *args, **kwargs):
        received_json_data = request.data
        start_date = received_json_data['start_date_statistics']
        stop_date = received_json_data['stop_date_statistics']

        result_count = Notification.objects.annotate(count_p=Count('message', filter=Q(message__created_at__range=
                                                                                       [start_date, stop_date],
                                                                                       message__status='Pn')),
                                                     count_d=Count('message', filter=Q(message__created_at__range=
                                                                                       [start_date, stop_date],
                                                                                       message__status='Dv')))
        result_statistics = []
        for pending_delivered in result_count:
            statistics_i = Statistics(
                notification=f'{pending_delivered.start_time} - {pending_delivered.end_time}',
                status=pending_delivered.status,
                pending=pending_delivered.count_p,
                delivered=pending_delivered.count_d)

            serializer_data = StatisticsSerializer(statistics_i)
            result_statistics.append(serializer_data.data)

        return Response({'code': status.HTTP_200_OK, 'result': result_statistics})


class OneStatisticsViewSet(APIView):
    @swagger_auto_schema(
        responses={200: StatisticsSerializer, 400: "Bad Request", 404: "Not Found"},
        operation_summary="Get one notification statistic",
        operation_description="Get one notification statistic by id .",
        tags=["statistics"],
    )
    def get(self, request, pk):
        result_count = Notification.objects.annotate(count_p=Count('message', filter=Q(
            message__status='Pn')), count_er=Count('message', filter=Q(message__status='Er')),
                                                     count_d=Count('message', filter=Q(message__status='Dv'))).get(
            id=pk)
        statistics_i = Statistics(
            notification=f'{result_count.start_time} - {result_count.end_time}',
            status=result_count.status,
            pending=result_count.count_p,
            delivered=result_count.count_d,
            error=result_count.count_er)
        serializer_data = StatisticsSerializer(statistics_i)
        return Response({'code': status.HTTP_200_OK, 'result': serializer_data.data})
