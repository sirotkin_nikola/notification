from django.db import models

STATUS_CHOICES = (
    ('Pn', 'Pending'),
    ('Dv', 'Delivered'),
    ('Er', 'Send_error')
)


class Client(models.Model):
    phone_number = models.CharField(max_length=200, verbose_name='номер телефона клиента')
    mobile_operator_code = models.CharField(max_length=200, verbose_name='код мобильного оператора')
    tag = models.CharField(max_length=200, verbose_name='произвольная метка клиента')
    timezone = models.CharField(max_length=200, verbose_name='часовой пояс клиента')

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return str(self.phone_number)


class Message(models.Model):
    notification = models.ForeignKey('Notification', on_delete=models.CASCADE,
                                     verbose_name='рассылка в рамках которого бфло отправлено сообщение')
    client = models.ForeignKey('Client', on_delete=models.CASCADE,
                               verbose_name='клиент которому было отправлено сообщение')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='дата и время создания', null=True, blank=True)
    delivered_at = models.DateTimeField(verbose_name='дата доставки', null=True, default=None, blank=True)
    status = models.CharField(max_length=200, choices=STATUS_CHOICES, verbose_name='статус отправки', default='Pending')
    error_message = models.CharField(max_length=200, verbose_name='сообщение об ошибке',
                                     null=True, blank=True, default='NO ERRORS')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return self.status


class Notification(models.Model):
    start_time = models.DateTimeField(verbose_name='дата и врремя запуска рассылки', null=False, )
    end_time = models.DateTimeField(verbose_name='дата и время окончания рассылки', null=False)
    message_text = models.CharField(max_length=200, verbose_name='текст сообщения для доставки клинту', default=None)
    client_filter_operator_code = models.CharField(max_length=200, verbose_name='код оператора клиента', default=None)
    client_filter_tag = models.CharField(max_length=200, verbose_name='тэг клиента', default=None)
    status = models.BooleanField(default=False, verbose_name='статус выполнения')

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return f'{self.start_time} - {self.end_time}'


class SendStatistics(Message):
    class Meta:
        proxy = True
        verbose_name = 'Статистика'
        verbose_name_plural = 'Статистики'
