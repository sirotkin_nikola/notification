from .models import *
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"


class NotificationSerializer(ModelSerializer):

    class Meta:
        model = Notification
        fields = "__all__"


class StatisticsSerializer(serializers.Serializer):
    notification = serializers.CharField()
    pending = serializers.IntegerField()
    delivered = serializers.IntegerField()
    error = serializers.IntegerField()
    status = serializers.BooleanField()
