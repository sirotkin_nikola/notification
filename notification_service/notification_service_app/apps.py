from django.apps import AppConfig


class NotificationServiceAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'notification_service_app'

    def ready(self):
        import notification_service_app.signals
