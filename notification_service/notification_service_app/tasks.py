from celery import Celery
import smtplib
from decouple import config
from notification_service_app.send_client_message import send_message
from notification_service_app.get_statistics_for_send import get_statistics
from .models import *
import datetime
from datetime import timezone
import logging
from celery.utils.log import get_task_logger
from notification_service.celery import logger_settings

app = Celery('myapp', broker=f'{config("REDIS_HOST")}://{config("REDIS_HOST")}:{config("REDIS_PORT")}/0')

logger = logger_settings()


@app.task
def send_client_if_created_correct_notification(data):
    result = send_message(data)
    logger.info(f'Send message to: {data[1]}, status code {result}')


@app.task
def send_message_if_time_has_come():
    notification_check = Notification.objects.filter(status=False)
    for notification in notification_check:
        if notification.start_time < datetime.datetime.now(timezone.utc) < notification.end_time:
            notification.status = True
            notification.save()
            client_send_message = Client.objects.filter(mobile_operator_code=notification.client_filter_operator_code,
                                                        tag=notification.client_filter_tag)
            for client in client_send_message:
                data = [client.id, int(client.phone_number), notification.message_text, notification.id]
                result = send_message(data)
                logger.info(f'Send message to: {client.phone_number}, status code {result}')


@app.task
def send_mail_celery():
    text = get_statistics()

    user = config("USER_SMTP_EMAIL")
    passwd = config("USER_SMTP_PASS")

    server = "smtp.yandex.ru"
    port = 587
    subject = "Тестовое письмо."
    to = config("USER_SEND_EMAIL")
    charset = 'Content-Type: text/plain; charset=utf-8'
    mime = 'MIME-Version: 1.0'

    logger.info(f'Отправлен email статистики на адрес: {to}')

    body = "\r\n".join((f"From: {user}", f"To: {to}",
                        f"Subject: {subject}", mime, charset, "", text))

    try:
        smtp = smtplib.SMTP(server, port)
        smtp.starttls()
        smtp.ehlo()
        smtp.login(user, passwd)
        smtp.sendmail(user, to, body.encode('utf-8'))
    except smtplib.SMTPException as err:
        logger.error(err)
        raise err
    except Exception as error:
        logger.error(error)
    finally:
        smtp.quit()

