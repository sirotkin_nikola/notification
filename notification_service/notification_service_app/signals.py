import datetime
from datetime import timezone
from django.db.models.signals import post_save
from django.dispatch import receiver
from notification_service_app.tasks import send_client_if_created_correct_notification
from .models import *


@receiver(post_save, sender=Notification)
def send_message_if_time(sender, instance, created, **kwargs):
    if created:
        if instance.start_time < datetime.datetime.now(timezone.utc) < instance.end_time:
            instance.status = True
            instance.save()
            client_send_message = Client.objects.filter(mobile_operator_code=instance.client_filter_operator_code,
                                                        tag=instance.client_filter_tag)
            for client in client_send_message:
                if datetime.datetime.now(timezone.utc) < instance.end_time:
                    data = [client.id, int(client.phone_number), instance.message_text, instance.id]
                    send_client_if_created_correct_notification.delay(data)
                else:
                    break
