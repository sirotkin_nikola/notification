import calendar
import datetime
import requests


def get_statistics():
    now = datetime.date.today()
    last_date = now - datetime.timedelta(days=now.day)
    last_month = last_date.month
    last_year = last_date.year

    end_day_of_month = calendar.monthrange(last_year, last_month)[1]
    json_data = {
        "start_date_statistics": str(last_date),
        "stop_date_statistics": str(last_date.replace(day=end_day_of_month)),
    }
    url = "http://127.0.0.1:8080/statistics/"

    headers = {
        'Content-Type': 'application/json',
    }

    response = requests.post(url, json=json_data, headers=headers, verify=False)
    return response.data
