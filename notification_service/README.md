# notification_service_app
## Создайте файл `.env`, заполните значения переменных, как показано в файле `.env_example`.

## Для сборки и запуска контейнера с приложением , перейдите в директрию с файлом docker-compose.yaml и введите команду:
```docker-compose up --build```

## Просмотрите запущенные контейнеры:
```docker ps -a```

## Подключитесь к контейнеру и создайте суперпользователя:
```docker exec -it container_id python manage.py createsuperuser```



## Notification endpoint: 
1. `/api/notification` Просмотр созданных рассылок, создание новой.
2. `/api/notification/{id}` Прочсмотр информации о рассылке, удаление, изменение.
## Client endpoint:
1. `/api/client` Просмотр списка клиентов, создание нового клиента.
2. `/api/client/{id}` Просмотр информации о клиенте, редактирование, удаление.
## Admin panel endpoint:
1. `/admin` Панель администратора.
## Swagger endpoint:
1. `/docs` Просмотр документации в формате OpenAPI.
## Statistics endpoint:
1. `/statistics` Получение статистики за указанный интервал даты и времени.
#### POST method:

```python
{
"start_date_statistics": "2011-01-01 00:00:00",
"stop_date_statistics": "2025-01-01 00:00:00"
}
```
2. `statistics/{id}` Получение статиски по выбранной рассылке,

### Первого числа, каждого месяца, собирается статистика за прошлый месяц и отправляется на указанный в `.env` файле email.
