from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from notification_service_app.views import (ClientViewSet, MessageViewSet, NotificationViewSet,
                                            StatisticsViewSet, OneStatisticsViewSet)
from django.conf import settings
from django.conf.urls.static import static
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

router = routers.SimpleRouter()
router.register(r'client', ClientViewSet)
router.register(r'notification', NotificationViewSet)

schema_view = get_schema_view(
    openapi.Info(
        title="Notification API",
        default_version='v1',
        description="Notification service API",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('statistics/', StatisticsViewSet.as_view()),
    path('statistics/<int:pk>', OneStatisticsViewSet.as_view()),
    path('swagger<format>/', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
